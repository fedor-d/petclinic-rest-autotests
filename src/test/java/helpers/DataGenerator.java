package helpers;

import com.github.javafaker.Faker;
import data.Owner;

import java.util.Locale;

public class DataGenerator {

    public static String getRandomFirstName() {

        return new Faker().name().firstName();
    }

    public static String getRandomLastName() {

        return new Faker().name().lastName();
    }

    public static String getRandomAddress() {

        Faker faker = new Faker(Locale.ENGLISH);
        return faker.address().fullAddress() + ' ' + faker.number().numberBetween(10, 100);
    }

    public static String getRandomCity() {

        return new Faker().address().city();
    }


    public static String getRandomTelephone() {

        return new Faker().phoneNumber().phoneNumber();
    }

    public static String getRandomOwnerTelephone() {

        return new Faker().number().digits(10);
    }


    public static Owner getRandomOwner() {

        Faker faker = new Faker();
        return Owner.builder().
                firstName(faker.name().firstName().toUpperCase()).
                lastName(faker.name().lastName().toLowerCase()).
                address(faker.address().fullAddress() + faker.random().nextInt(50, 100)).
                city(faker.address().city()).
                telephone(faker.phoneNumber().cellPhone()).
                build();
    }
}
