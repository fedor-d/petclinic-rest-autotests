// # Keep this file simple!

function fn() {
    var env = karate.env; // get system property 'karate.env'
    karate.log('karate.env system property was:', env);
    if (!env) {
        env = 'aws';
    }
    var config = {
        env: env,

    }
    if (env == 'local') {

        config.userName = 'admin'
        config.userPassword = 'admin'
        config.ownersApiUrl = 'http://localhost:9966/petclinic/api/'
    } else if (env == 'staging') {

        config.userName = 'staging-admin'
        config.userPassword = 'staging-admin'
    } else if (env == 'aws') {
        config.ownersApiUrl = 'http://ec2-13-213-66-3.ap-southeast-1.compute.amazonaws.com:8081/petclinic/api/'
    }

    config.faker = Java.type('com.github.javafaker.Faker');

    return config;
}