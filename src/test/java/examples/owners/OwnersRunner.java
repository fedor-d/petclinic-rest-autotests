package examples.owners;

import com.intuit.karate.junit5.Karate;

class OwnersRunner {
    
    @Karate.Test
    Karate testUsers() {
        return Karate.run("owners").relativeTo(getClass());
    }    

}
