@parallel=false
Feature: CRUD Tests of Owners Spring Pet Clinic REST API

  # Variables set using def in the Background will be re-set before every Scenario
  Background: Preconditions
    Given url ownersApiUrl
    Given path 'owners'

    # define the simplest JavaScript Function
    * def deleteMeLastName =
    """
      function(isDeleteLastName) {
        return isDeleteLastName ? "Delete-Me-Please" : "Don't Delete Me!";
        }
    """

    # use JS Function and print the return value to console
    * def delLastName = deleteMeLastName(true)
    * print delLastName

    # define DataGenerator class using
    # powerful library to make random data https://github.com/DiUS/java-faker
    # JS alternative is https://github.com/marak/Faker.js/
    * def dataGenerator = Java.type('helpers.DataGenerator')


# -------_______-------
  Scenario: 1 get all owners and then get the first owner by id
    When method get
    Then status 200

    * def first = response[0]

    Given path 'owners', first.id
    When method get
    Then status 200

# -------_______-------
  Scenario: 2 create Martin Silenus owner and check the response
    Given request {"firstName":"Martin","lastName":"Silenus","address":"Peshkov street","city":"Tau Ceti Center","telephone":"01234567890"}
    When method post
    Then status 201

    # collect the POST's response to a var
    * def createdOwner = response

    # let's create an expectedOwner
    * def expectedOwner = {firstName: 'Martin'}

    # how we can manipulate JSON
    * expectedOwner.lastName = 'Silenus'
    * expectedOwner.telephone = '01234567890'

    # use set
    * set expectedOwner.address = 'Peshkov street'

    # manipulating data by using pure JsonPaths expressions
    * set expectedOwner $.city = 'Tau Ceti Center'

    # print variables into console
    * print 'the value of expectedOwner is: ', expectedOwner

    Then match createdOwner.firstName == "#string"
    And match createdOwner.lastName !contains 'Chupacabra'
    And match createdOwner ==
  """
  {
    id: '#ignore',
    firstName: '#regex ^[J-P]\\D{3}i?n',
    lastName: 'Silenus',
    address: 'Peshkov street',
    city: 'Tau Ceti Center',
    telephone: '01234567890',
    pets: '#array'
  }
  """

# -------_______-------
  Scenario: 3 update the Martin Silenus owner entity by Test Data Generator

    # find all owners with last name Silenus
    Given path '/*/lastname/Silenus'
    And method get

    * def silenus = response[0]

    # Use faker by "explicit" Java class
    * def randomFirstName = dataGenerator.getRandomFirstName()
#    * def randomLastName = dataGenerator.getRandomLastName() + ' delete me'
    * def randomAddress = dataGenerator.getRandomAddress()
    * def randomTelephone = dataGenerator.getRandomOwnerTelephone()

#    * def randomTelephone = fakerObj.phoneNumber().phoneNumber()


    # Use faker by Java inside karate-config.js
    * def fakerObj = new faker()
    * def randomCity = fakerObj.address().city()


    Given url ownersApiUrl
    And path 'owners', silenus.id

    # let's use JS function deleteMeLastName below
    And request
      """
        {
          firstName: #(randomFirstName),
          lastName: #(deleteMeLastName(true)),
          address: #(randomAddress),
          city: #(randomCity),
          telephone: #(randomTelephone),
          pets: []
        }
      """

    When method put
    Given url ownersApiUrl
    And path 'owners', silenus.id
    Then status 204


    When method get
    Given url ownersApiUrl
    And path 'owners', silenus.id
    Then status 200

    * def updatedSilenus = response

    And match updatedSilenus.firstName == randomFirstName
    And match updatedSilenus.city == randomCity


  # -------_______-------
  Scenario: 4 delete the previous scenario's owner entity
    # let's use delLastName defined in Background
    Given path '/*/lastname/' + delLastName
    And method get
    Then status 200

    * def delEntityId = response[0].id

    Given path 'owners', delEntityId
    And method delete
    Then status 204

    Given path 'owners', delEntityId
    And method get
    Then status 404

  # -------_______-------
  Scenario Outline: positive data driven scenario

    * def expectedResponse =
  """
    {
       field123:'#notpresent',
       id:'#number',
       firstName:'##string',
       lastName:'#string',
       address:'#string',
       city:'#string',
       telephone:'#string',
       pets:'#present'
     }
  """
    Given request
    """
    {
      firstName: "<firstName>",
      lastName: "<lastName>",
      address: "<address>",
      city: "<city>",
      telephone: "<phone>",
      pets: []
    }
    """
    And method post

    Then status 201
    And match response == <actualResponse>


    Examples:
        |firstName|lastName   |address    |city     |phone        | actualResponse
        |name1    |family1    |address1   |city1    |0123456789   |expectedResponse
        |name2    |family2    |address2   |city2    |9876543210   |expectedResponse
        |name3    |family3    |address3   |city3    |0123456789   |expectedResponse
        |name5    |family5    |address5   |city5    |9876543210   |expectedResponse
        |name999  |family999  |address999 |city999  |0123456789   |expectedResponse


