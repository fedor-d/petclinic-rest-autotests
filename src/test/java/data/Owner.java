package data;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Owner {

    String firstName;
    String lastName;
    String address;
    String city;
    String telephone;
}
